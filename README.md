Ansible Role to manage RClone installation
------------------------------------------

This simple role allow you to install [RClone](https://rclone.org/) on a specific machine.

## Example

```ansible-playbook
- name: Install RClone
  include_role:
    name: rclone
```
